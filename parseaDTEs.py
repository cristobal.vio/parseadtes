import xml.etree.ElementTree as ET
import glob
import os
#import argparse
from itertools import cycle
from datetime import date
import re

tasaIVA = 0.19

def digito_verificador(rut):
    reversed_digits = map(int, reversed(str(rut)))
    factors = cycle(range(2, 8))
    s = sum(d*f for d,f in zip(reversed_digits, factors))
    return (-s) % 11

class DTEparser():
    def cast_int(self,el):
        return int(el.text)
    
    def cast_float(self,el):
        return float(el.text)
    
    def cast_text(self,el):
        return el.text
    
    def cast_rut(self,el):
        return int(el.text.split('-')[0])
    
    def cast_date(self,el):
        return date.fromisoformat(el.text)
    
    def cast_code(self,el):
        if el.text is not None:
            r = int(el.text)
        else:
            r = None
        return r

    firm_attrs = {
            'emisor':{
                'RUT':{
                    'func'     : cast_rut,
                    'xpath'    : "./RUTEmisor"
                    },
                'rSocial':{
                    'func'     : cast_text,
                    'xpath'    : "./RznSoc"
                    },
                'dire':{
                    'func'     : cast_text,
                    'xpath'    : "./DirOrigen"
                    },
                'giro':{
                    'func'     : cast_text,
                    'xpath'    : "./GiroEmis"
                    }
                },
            'receptor':{
                'RUT':{
                    'func'     : cast_rut,
                    'xpath'    : "./RUTRecep"
                    },
                'rSocial':{
                    'func'     : cast_text,
                    'xpath'    : "./RznSocRecep"
                    },
                'dire':{
                    'func'     : cast_text,
                    'xpath'    : "./DirRecep"
                    },
                'giro':{
                    'func'     : cast_text,
                    'xpath'    : "./GiroRecep"
                    }
                }
            }
    firms = {}

    def cast_empresa(self,el):
        fa = self.firm_attrs
        ns = self.namespace
        role = ''
        if   el.tag.find('Emisor')   != -1:
            role = 'emisor'
        elif el.tag.find('Receptor') != -1:
            role = 'receptor'
        rutEl = el.find(self.i_ns(fa[role]['RUT']['xpath']))
        rutE  = int(rutEl.text.split('-')[0])
        if rutE in self.firms:
            return self.firms[rutE]
        else:
            res = {}
            for a in fa[role]:
                if (fae := el.find(self.i_ns(fa[role][a]['xpath']))) is not None:
                    res[a] = fa[role][a]['func'](self,fae)
                else:
                    res[a] = None
            e = empresa(**res)
            self.firms[e.RUT] = e
            return e

    attrs_toparse = {
            'tipo':{
                'func'     : cast_int,
                'xpath'    : ".//IdDoc/TipoDTE"
                },
            'num':{
                'func'     : cast_int,
                'xpath'    : ".//IdDoc/Folio"
                },
            'fecha':{
                'func'     : cast_date,
                'xpath'    : ".//IdDoc/FchEmis"
                },
            'vence':{
                'func'     : cast_date,
                'xpath'    : ".//IdDoc/FchVenc"
                },
            'emisor':{
                'func'     : cast_empresa,
                'xpath'    : ".//Emisor"
                },
            'receptor':{
                'func'     : cast_empresa,
                'xpath'    : ".//Receptor"
                },
            'montoNeto':{
                'func'     : cast_float,
                'xpath'    : ".//MntNeto"
                },
            'montoEx':{
                'func'     : cast_float,
                'xpath'    : ".//MntExe"
                },
            'montoNF':{
                'func'     : cast_float,
                'xpath'    : ".//MontoNF"
                }
            }
    itemattrs_toparse = {
            'nro_item':{
                'func'     : cast_int,
                'xpath'    : "./NroLinDet"
                },
            'nombre':{
                'func'     : cast_text,
                'xpath'    : "./NmbItem"
                },
            'monto':{
                'func'     : cast_float,
                'xpath'    : "./MontoItem"
                },
            'codigo':{
                'func'     : cast_code,
                'xpath'    : "./CdgItem/VlrCodigo"
                },
            'unidad':{
                'func'     : cast_text,
                'xpath'    : "./UnmdItem"
                },
            'cantidad':{
                'func'     : cast_float,
                'xpath'    : "./QtyItem"
                },
            'precio':{
                'func'     : cast_float,
                'xpath'    : "./PrcItem"
                }
            }

    def __init__(self,xmlfiles,categories=['emisor','tipo','num']):
        cnc = categories.count('num')
        if cnc > 1:
            raise ValueException("categories list argument may only have one 'num' element")
        elif cnc == 1:
            categories.pop(categories.index('num'))
        categories.append('num') # the num category goes last
        self.docs:       dict  = {}
        self.categories: [str] = categories
        self.files:      str   = xmlfiles
        self.firms:      dict  = {}

    def i_ns(self,s):
        ns = self.namespace
        return re.sub(r'([^a-zA-Z]*/+)([^/]*)',r'\1'+r'{'+ns+r'}'+r'\2',s)

    def organize(self,fact,d,c):
        k = c[0]
        if len(c)==1:
            d[repr(getattr(fact,k))]=fact
        else:
            if repr(getattr(fact,k)) not in d:
                d[repr(getattr(fact,k))] = {}
            self.organize(fact,d[repr(getattr(fact,k))],c[1:])

    def getDTEs(self):
        for filename in glob.glob(os.path.join('./', self.files)):
            self.parseXML(filename)

    def parseXML(self,file):
        self.tree = ET.parse(file)
        print(f"parsing {file}")
        self.root = self.tree.getroot()
        self.namespace = self.root.tag.split('}')[0].strip('{')
        for child in self.root.findall(".//{"+self.namespace+"}Documento"):
            documento = self.getDTE(child)
            self.organize(documento,self.docs,self.categories)

    def getDTE(self,el):
        ns = self.namespace
        res = {}
        ats = self.attrs_toparse
        iats = self.itemattrs_toparse
        for a in ats:
            if (ael := el.find(self.i_ns(ats[a]['xpath']))) is not None:
                res[a] = ats[a]['func'](self,ael)
            else:
                res[a] = None
                print(f"Attribute {a} not found under xpath {self.i_ns(ats[a]['xpath'])}")
        res_its = []
        if (its := el.findall(self.i_ns(".//Detalle"))) is not None:
            for i in its:
                i_vals = {}
                for a in iats:
                    if (iel := i.find(self.i_ns(iats[a]['xpath']))) is not None:
                        i_vals[a] = iats[a]['func'](self,iel)
                    else:
                        i_vals[a] = None
                res_its.append(docItem(**i_vals))
        res['items'] = res_its
        return self.call_constructor(res)

    def call_constructor(self,res):
        return dte(**res)

class CGE_DTEparser(DTEparser):
    attrs_toparse = DTEparser.attrs_toparse.copy()

    def __init__(self,xmlfiles,categories=['nserv','tipo','num']):
        super().__init__(xmlfiles,categories)
        self.attrs_toparse.update(
                {
                'nserv':{
                    'func'     : DTEparser.cast_int,
                    'xpath'    : ".//CdgIntRecep"
                    }
                }
            )

    def from_dte(self,this_dte):
        atts = {}
        for a in self.attrs_toparse:
        #   if a != 'nserv':
                atts[a]=getattr(this_dte,a)
        for i in this_dte.items:
            if (rem := re.match('Electricidad Consumida .([0-9.]+) kWh.',i.nombre)) is not None:
                atts['Ea'] = int(rem.groups()[0].replace('.',''))
                break
        atts['items'] = this_dte.items
        return dteCGE(**atts)

    def getDTE(self,el):
        this_dte = super().getDTE(el)
        return self.from_dte(this_dte)

    def call_constructor(self,res):
        return dteCGE(**res)

class dte():

    def __init__(self,tipo,num,fecha,vence,emisor,receptor,montoNeto,items,montoEx=0,montoNF=0):
        self.tipo:          int = tipo
        self.num:           int = num
        self.fecha:        date = fecha
        self.vence:        date = vence
        self.emisor:    empresa = emisor
        self.receptor:  empresa = receptor
        self.montoNeto:   float = montoNeto # Monto Neto
        self.montoEx:     float = montoEx   # Monto Exento
        self.montoNF:     float = montoNF   # Monto No Facturable
        self.items:   [docItem] = items

    def obtenerIVA(self):
        return self.montoNeto*tasaIVA

    def obtenerTotal(self):
        return self.montoNeto*(1+tasaIVA) + self.mntex - self.mntnf

    def addItem(self,item):
        self.items.append(item)

   #def __repr__(self):
   #    return repr(self.num)

class dteCGE(dte):
    def __init__(self,tipo,num,fecha,vence,emisor,receptor,montoNeto,items,nserv,montoEx=0,montoNF=0,Ea=None):
        super().__init__(tipo,num,fecha,vence,emisor,receptor,montoNeto,items)
        self.nserv = nserv
        self.Ea    = Ea

class docItem():
    def __init__(self,nro_item,nombre,monto,codigo,unidad,cantidad,precio):
        self.nro_item: int   = nro_item     # These three attributes are
        self.nombre:   str   = nombre       # always present, unlike the
        self.monto:    float = monto        # ones that follow, which may not
        self.codigo:   str   = codigo
        self.unidad:   str   = unidad
        self.cantidad: float = cantidad
        self.precio:   float = precio

    def __repr__(self):
        return repr(self.nombre)

class empresa():
    def __init__(self,RUT,rSocial,dire,giro):
        self.RUT:     int = RUT
        self.rSocial: str = rSocial
        self.dire:    str = dire
        self.giro:    str = giro

    def __repr__(self):
        return repr(self.RUT)

